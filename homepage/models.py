from django.db import models
import django.utils.timezone


# Create your models here.
class Schedule1(models.Model):

    title = models.CharField(max_length=100)
    place = models.CharField(max_length=100, null=True)
    category = models.CharField(
        max_length=8
    )
   
    date = models.DateField(auto_now_add = False, default = django.utils.timezone.now)
    day = models.CharField(max_length=10, default=None, null=True)
    time = models.TimeField(auto_now_add = False, default = django.utils.timezone.now)

    description = models.CharField(max_length=100, default = "")

    def __str__(self):
        return self.title