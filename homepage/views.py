from django.shortcuts import render, redirect
from . import forms
from .models import Schedule1


# Create your views here.
def home(request):
	return render(request, "ppwHome1.html")

def about(request):
	return render(request, "ppwAbout.html")

def skills(request):
	return render(request, "ppwSkills.html")

def hobby(request):
	return render(request, "ppwHobby.html")

def contact(request):
	return render(request, "ppwContact.html")

def music(request):
	return render(request, "ppwMusic.html")

# Create your views here.

def schedule_create(request):
    if request.method == "POST":
        form = forms.Schedule(request.POST)

        if form.is_valid():   
            new_sched = form.save(commit=False)
            new_sched.day = new_sched.date.strftime("%A")
            new_sched.save()
        
        return redirect("homepage:schedule")
    else:
        form = forms.Schedule()
        return render(request, "schedule.html", {"form" : form, "schedules": Schedule1.objects.all().order_by("date")})

def schedule_delete(request, id):
    jadwal = Schedule1.objects.get(id=id)
    jadwal.delete()
    return redirect("homepage:schedule")
