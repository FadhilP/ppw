from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('experience/', views.skills, name='skills'),
    path('hobby/', views.hobby, name='hobby'),
    path('contact/', views.contact, name='contact'),
    path('music/', views.music, name='music'),
    path('schedule/', views.schedule_create, name="schedule"),
    path('delete/<id>/', views.schedule_delete, name="schedule_delete")
]