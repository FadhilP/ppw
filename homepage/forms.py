from django import forms
from . import models
from django.utils import timezone
from datetime import datetime

class Time(forms.TimeInput):
    input_type = "time"

class Date(forms.DateInput):
    input_type = "date"
    input_formats = ['%d %b %Y']


class Schedule(forms.ModelForm):
    class Meta:
        model = models.Schedule1
        fields = ['title', 'place', 'category', 'date', 'time', 'description']

        widgets = {
            "time": Time,
            "date": Date
        }


